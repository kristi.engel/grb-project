clear
HAWC_long = -97.3; % deg. west
HAWC_lat = 19; % deg.
namesGRB = cell(2000,5);

myDir = '/Users/kristiengel/Documents/MATLAB/zGRBs/'; %gets directory
myFiles = dir(fullfile(myDir,'*.csv')); %gets all txt files in struct
warning('off','all');
%figure1 = figure;
for c = 1:length(myFiles)
    clear GRB Y MO D H MI S hRA mRA sRA dDec amDec asDec redZ GMST hLMST...
        degAzi degDec degDiff degHrAng degRA degsinAzi degZ hLMST_l LMST...
        mLMST mLMST_l sLMST tmpdegDiff tmpdegHrAng tmpGMST jd degcosZ
  baseFileName = myFiles(c).name;
  fullFileName = fullfile(myDir, baseFileName);
  
  fprintf(1, 'Now reading %s\n', fullFileName);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName);
    
    %namesGRB =  zeros([2000 c]);
    %namesGRB = cell(2000,5);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    %tmpChannelA = str2double(test.ChannelA(2:end));
    GRB = test.Name(1:end);
    Y = test.Year(1:end);
    MO = test.Month(1:end);
    D = test.Day(1:end);
    H = test.hUTC(1:end);
    MI = test.mUTC(1:end);
    S = test.sUTC(1:end);
    hRA = test.hRA(1:end);
    mRA = test.mRA(1:end);
    sRA = test.sRA(1:end);
    dDec = test.dDec(1:end);
    amDec = test.amDec(1:end);
    asDec = test.asDec(1:end);
    redZ = test.redZ(1:end); 
    
    i = 1;
    j = 0;
    j1 = 0;
    j2 = 0;
    k = 0;
    k1 = 0;
    k2 = 0;
    n = 0;
    red1 = 0; % # w/ redshift overall
    red12 = 0; % # w/ redshift < 1 overall
    red13 = 0; % # w/ redshift < 0.3 overall
    red2 = 0; % # w/ redshift w/in 45 deg
    red22 = 0; % # w/ redshift < 1 w/in 45 deg
    red23 = 0; % # w/ redshift < 0.3 w/in 45 deg
    red3 = 0; % # w/ redshift w/in 30 deg
    red32 = 0; % # w/ redshift < 1 w/in 30 deg
    red33 = 0; % # w/ redshift < 0.3 w/in 30 deg
    red4 = 0; % # w/ redshift w/in 15 deg
    red42 = 0; % # w/ redshift < 1 w/in 15 deg
    red43 = 0; % # w/ redshift < 0.3 w/in 15 deg
    red5 = 0; % # w/ redshift w/in ~3 hr
    red52 = 0; % # w/ redshift < 1 w/in ~3 hr
    red53 = 0; % # w/ redshift < 0.3 w/in ~3 hr
    red6 = 0; % # w/ redshift w/in ~2 hr
    red62 = 0; % # w/ redshift < 1 w/in ~2 hr
    red63 = 0; % # w/ redshift < 0.3 w/in ~2 hr
    red7 = 0; % # w/ redshift w/in ~1 hr
    red72 = 0; % # w/ redshift < 1 w/in ~1 hr
    red73 = 0; % # w/ redshift < 0.3 w/in ~1 hr
    for i = 1:length(Y)
        % Put name in big name array
        namesGRB(i,c) = GRB(i);
        
        % Check for any redshift at all
        if redZ(i) > 0
            red1 = red1 + 1;
            if redZ(i) < 1
                red12 = red12 + 1;
                if redZ(i) < 0.3
                    red13 = red13 + 1;
                end
            end
        end
        
        % Convert GRB coordinates to degrees
        degRA(i) = (hRA(i) + ((mRA(i) + (sRA(i)/60))/60))*15;
        if dDec(i) > 0
            degDec(i) = dDec(i) + ((amDec(i) + (asDec(i)/60))/60);
        elseif dDec(i) <  0
            degDec(i) = ((dDec(i)*(-1)) + ((amDec(i) + (asDec(i)/60))/60))*(-1);
        else
            degDec(i) = dDec(i) + ((amDec(i) + (asDec(i)/60))/60);
        end
        
        % Want to convert the UTC date and time to the Local Mean Sidereal Time at
        % HAWC in hr-min-sec
        jd(i) = juliandate([Y(i),MO(i),D(i),H(i),MI(i),S(i)]);
        tmpGMST(i) = siderealTime(jd(i)); % Note: in degrees
            if tmpGMST(i) < abs(HAWC_long)
                msgGMST = [GRB{i},' is weird.'];
                %disp(msgGMST)
                
                GMST(i) = tmpGMST(i) + 360;
                %disp(tmpGMST(i))
            else
                GMST(i) = tmpGMST(i);
                
             end
                
                LMST(i) = GMST(i) + HAWC_long; % in degrees
                hLMST_l(i) = LMST(i)/15;
                hLMST(i) = floor(hLMST_l(i));
                mLMST_l(i) = 60*(hLMST_l(i) - hLMST(i));
                mLMST(i) = floor(mLMST_l(i));
                sLMST(i) = 60*(mLMST_l(i) - mLMST(i));
                
                % Zenith/azimuth  conversions
                tmpdegHrAng(i) = LMST(i) - degRA(i);
                    if tmpdegHrAng(i) < -180
                        degHrAng(i) = tmpdegHrAng(i) + 360;
                    elseif tmpdegHrAng(i) > 180
                        degHrAng(i) = tmpdegHrAng(i) - 360;
                    else
                        degHrAng(i) = tmpdegHrAng(i);
                    end
                degcosZ(i) = (cosd(HAWC_lat)*cosd(degDec(i))*cosd(degHrAng(i))) + (sind(HAWC_lat)*sind(degDec(i)));
                degZ(i) = acosd(degcosZ(i)); % zenith angle, in degrees
                degsinAzi(i) = (-cosd(degDec(i))*sind(degHrAng(i)))/(sind(degZ(i)));
                degAzi(i) = asind(degsinAzi(i));
        
                %hDiff(i) = hRA(i) - hLMST(i);
                tmpdegDiff(i) = degRA(i) - LMST(i);
                if tmpdegDiff(i) < -180
                    degDiff(i) = tmpdegDiff(i) + 360;
                elseif tmpdegDiff(i) > 180
                    degDiff(i) = tmpdegDiff(i) - 360;
                else
                    degDiff(i) = tmpdegDiff(i);
                end
                
                %Need to cut on Zenith angle to determine if the GRB was in
                %HAWC's field of view
                if degZ(i) < 15
                    j1 = j1 + 1;
                    if redZ(i) > 0
                        red4 = red4 + 1;
                        Red_yes1 = [GRB{i},' has a known redshift value of ',num2str(redZ(i)),'.'];
                        %disp(Red_yes1)
                        if redZ(i) < 1
                            red42 = red42 + 1;
                            Red_yes2 = [GRB{i},' has a known redshift value of ',num2str(redZ(i)),'.'];
                            %disp(Red_yes2)
                            if redZ(i) < 0.3
                                red43 = red43 + 1;
                                Red_yes3 = [GRB{i},' has a known redshift value of ',num2str(redZ(i)),'.'];
                                %disp(Red_yes3)
                            end
                        end
                    end
                    FoV_yes1 = [GRB{i},' was in HAWC''s field of view at the start of its reported T90 window; zen < 15 deg.'];
                    %disp(FoV_yes1)
                    FoV_y1Stats = ['The zenith angle for ',GRB{i},' was ',num2str(degZ(i)),', and the azimuthal angle was ',num2str(degAzi(i)),'.'];
                    %disp(FoV_y1Stats)
                elseif degZ(i) < 30
                    j2 = j2 + 1;
                    if redZ(i) > 0
                        red3 = red3 + 1;
                        Red_yes1 = [GRB{i},' has a known redshift value of ',num2str(redZ(i)),'.'];
                        %disp(Red_yes1)
                        if redZ(i) < 1
                            red32 = red32 + 1;
                            Red_yes2 = [GRB{i},' has a known redshift value of ',num2str(redZ(i)),'.'];
                            %disp(Red_yes2)
                            if redZ(i) < 0.3
                                red33 = red33 + 1;
                                Red_yes3 = [GRB{i},' has a known redshift value of ',num2str(redZ(i)),'.'];
                                %disp(Red_yes3)
                            end
                        end
                    end
                    FoV_yes2 = [GRB{i},' was in HAWC''s field of view at the start of its reported T90 window; 15 < zen < 30 deg.'];
                    %disp(FoV_yes2)
                    FoV_y2Stats = ['The zenith angle for ',GRB{i},' was ',num2str(degZ(i)),', and the azimuthal angle was ',num2str(degAzi(i)),'.'];
                    %disp(FoV_y2Stats)
                elseif degZ(i) < 45
                    j = j + 1;
                    if redZ(i) > 0
                        red2 = red2 + 1;
                        Red_yes1 = [GRB{i},' has a known redshift value of ',num2str(redZ(i)),'.'];
                        %disp(Red_yes1)
                        if redZ(i) < 1
                            red22 = red22 + 1;
                            Red_yes2 = [GRB{i},' has a known redshift value of ',num2str(redZ(i)),'.'];
                            %disp(Red_yes2)
                            if redZ(i) < 0.3
                                red23 = red23 + 1;
                                Red_yes3 = [GRB{i},' has a known redshift value of ',num2str(redZ(i)),'.'];
                                %disp(Red_yes3)
                            end
                        end
                    end
                    FoV_yes = [GRB{i},' was in HAWC''s field of view at the start of its reported T90 window; 30 < zen < 45 deg.'];
                    %disp(FoV_yes)
                    FoV_yStats = ['The zenith angle for ',GRB{i},' was ',num2str(degZ(i)),', and the azimuthal angle was ',num2str(degAzi(i)),'.'];
                    %disp(FoV_yStats)
                elseif degZ(i) < 60 && (degDiff(i) > 0) && (degRA(i) < LMST(i)) && degDec(i) < 65 && degDec(i) > -28
                    k1 = k1 + 1;
                    if redZ(i) > 0
                        red7 = red7 + 1;
                        Red_yes1 = [GRB{i},' has a known redshift value of ',num2str(redZ(i)),'.'];
                        %disp(Red_yes1)
                        if redZ(i) < 1
                            red72 = red72 + 1;
                            Red_yes2 = [GRB{i},' has a known redshift value of ',num2str(redZ(i)),'.'];
                            %disp(Red_yes2)
                            if redZ(i) < 0.3
                                red73 = red73 + 1;
                                Red_yes3 = [GRB{i},' has a known redshift value of ',num2str(redZ(i)),'.'];
                                %disp(Red_yes3)
                            end
                        end
                    end
                    FoV_close1 = [GRB{i},' moved into HAWC''s field of view within ~1 hours after the start of its reported T90 window.'];
                    %disp(FoV_close1)
                    FoV_c1Stats = ['The zenith angle for ',GRB{i},' was ',num2str(degZ(i)),', and the azimuthal angle was ',num2str(degAzi(i)),'.'];
                    %disp(FoV_c1Stats)
                elseif degZ(i) < 70 && (degDiff(i) > 0) && (degRA(i) < LMST(i)) && degDec(i) < 65 && degDec(i) > -28
                    k2 = k2 + 1;
                    if redZ(i) > 0
                        red6 = red6 + 1;
                        Red_yes1 = [GRB{i},' has a known redshift value of ',num2str(redZ(i)),'.'];
                        %disp(Red_yes1)
                        if redZ(i) < 1
                            red62 = red62 + 1;
                            Red_yes2 = [GRB{i},' has a known redshift value of ',num2str(redZ(i)),'.'];
                            %disp(Red_yes2)
                            if redZ(i) < 0.3
                                red63 = red63 + 1;
                                Red_yes3 = [GRB{i},' has a known redshift value of ',num2str(redZ(i)),'.'];
                                %disp(Red_yes3)
                            end
                        end
                    end
                    FoV_close2 = [GRB{i},' moved into HAWC''s field of view within ~2 hours after the start of its reported T90 window.'];
                    %disp(FoV_close2)
                    FoV_c2Stats = ['The zenith angle for ',GRB{i},' was ',num2str(degZ(i)),', and the azimuthal angle was ',num2str(degAzi(i)),'.'];
                    %disp(FoV_c2Stats)
                elseif degZ(i) < 80 && (degDiff(i) > 0) && (degRA(i) < LMST(i)) && degDec(i) < 65 && degDec(i) > -28
                    k = k + 1;
                    if redZ(i) > 0
                        red5 = red5 + 1;
                        Red_yes1 = [GRB{i},' has a known redshift value of ',num2str(redZ(i)),'.'];
                        %disp(Red_yes1)
                        if redZ(i) < 1
                            red52 = red52 + 1;
                            Red_yes2 = [GRB{i},' has a known redshift value of ',num2str(redZ(i)),'.'];
                            %disp(Red_yes2)
                            if redZ(i) < 0.3
                                red53 = red53 + 1;
                                Red_yes3 = [GRB{i},' has a known redshift value of ',num2str(redZ(i)),'.'];
                                %disp(Red_yes3)
                            end
                        end
                    end
                    FoV_close = [GRB{i},' moved into HAWC''s field of view within ~3 hours after the start of its reported T90 window.'];
                    %disp(FoV_close)
                    FoV_cStats = ['The zenith angle for ',GRB{i},' was ',num2str(degZ(i)),', and the azimuthal angle was ',num2str(degAzi(i)),'.'];
                    %disp(FoV_cStats)
                elseif degZ(i) < 80 && degDec(i) < 65 && degDec(i) > -28
                    n = n + 1;
                    FoV_close3 = [GRB{i},' was in HAWC''s field of view within ~3 hours before the start of its reported T90 window.'];
                    %disp(FoV_close3)
                    FoV_c3Stats = ['The zenith angle for ',GRB{i},' was ',num2str(degZ(i)),', and the azimuthal angle was ',num2str(degAzi(i)),'.'];
                    %disp(FoV_c3Stats)
                else
                    FoV_no = [GRB{i},' was not in HAWC''s field of view at, or within 3 hours of, the start of its reported T90 window.'];
                    %disp(FoV_no)
                    FoV_nStats = ['The zenith angle for ',GRB{i},' was ',num2str(degZ(i)),', and the azimuthal angle was ',num2str(degAzi(i)),'.'];
                    %disp(FoV_nStats)
                end
                
                % Need to compare this LMST to the source RA to estimate if the GRB was in
                % HAWC's field of view at the start of T90
                %if abs(degDiff(i)) < 45
                 %   j = j + 1;
                  %  FoV_yes = [GRB{i},' was in HAWC''s field of view at the start of its reported T90 window.'];
                    %disp(FoV_yes)
                   % FoV_yStats = ['The RA for ',GRB{i},' was ',num2str(hRA(i)),'h',num2str(mRA(i)),'m',num2str(sRA(i)),'s (',num2str(degRA(i)),'), and the LMST of HAWC was ',num2str(hLMST(i)),'h',num2str(mLMST(i)),'m',num2str(sLMST(i)),'s (',num2str(LMST(i)),').'];
                    %disp(FoV_yStats)
                    %disp(abs(hDiff(i)))
                %elseif abs(degDiff(i)) < 75
                 %   k = k + 1;
                  %  FoV_close = [GRB{i},' was in HAWC''s field of view within 3 hours of the start of its reported T90 window.'];
                   % disp(FoV_close)
                   % if (degDiff(i) > 0) && (degRA(i) < LMST(i))
                    %    FoV_vclose = [GRB{i},' precedes the HAWC field of view.'];
                     %   disp(FoV_vclose)
                   %end
                    %FoV_cStats = ['The RA for ',GRB{i},' was ',num2str(hRA(i)),'h',num2str(mRA(i)),'m',num2str(sRA(i)),'s (',num2str(degRA(i)),'), and the LMST of HAWC was ',num2str(hLMST(i)),'h',num2str(mLMST(i)),'m',num2str(sLMST(i)),'s (',num2str(LMST(i)),').'];
                    %disp(FoV_cStats)

                %else
                 %   FoV_no = [GRB{i},' was not in HAWC''s field of view at, or within 3 hours of, the start of its reported T90 window.'];
                    %disp(FoV_no)
                %end
          
    end
    
   % Weird edge case to check
    %disp(GRB{4})
    %disp(hRA(4))
    %disp(hLMST(4))
    %degRA(4)
    
    len = length(Y);
    j3 = j + j1 + j2;
    j4 = j1 + j2;
    k3 = k + k1 + k2;
    k4 = k1 + k2;
    redsum1 = red4 + red3;
    redsum12 = red42 + red32;
    redsum13 = red43 + red33;
    redsum2 = red2 + redsum1;
    redsum22 = red22 + redsum12;
    redsum23 = red43 + redsum13;
    redsum3 = red7 + red6;
    redsum32 = red72 + red62;
    redsum33 = red73 + red63;
    redsum4 = red5 + redsum3;
    redsum42 = red52 + redsum32;
    redsum43 = red53 + redsum33;
    %fprintf(1, '%4.0f total GRB(s) in %s\n', len, fullFileName);
    fprintf(1, '%4.0f total GRB(s) in %s\n', len, baseFileName);
        if len > 0
            fprintf(1, '     %4.0f of these have a known redshift\n', red1);
        end
        if red12 > 0
            fprintf(1, '     %4.0f of those redshifts are < 1\n', red12);
        end
        if red13 > 0
            fprintf(1, '     %4.0f of those redshifts are < 0.3\n', red13);
        end
    fprintf(1, '  %4.0f visible GRB(s) within 45 deg zenith\n', j3);
    %fprintf(1, '  %4.0f visible GRB(s) within 45 deg zenith in %s\n', j3, fullFileName);
        if j3 > 0
            fprintf(1, '      %4.0f of these have a known redshift\n', redsum2);
        end
        if redsum22 > 0
            fprintf(1, '      %4.0f of those redshifts are < 1\n', redsum22);
        end
        if redsum23 > 0
            fprintf(1, '      %4.0f of those redshifts are < 0.3\n', redsum23);
        end
    fprintf(1, '  %4.0f visible GRB(s) within 30 deg zenith\n', j4);
    %fprintf(1, '  %4.0f visible GRB(s) within 30 deg zenith in %s\n', j4, fullFileName);
        if j4 > 0
            fprintf(1, '      %4.0f of these have a known redshift\n', redsum1);
        end
        if redsum12 > 0
            fprintf(1, '      %4.0f of those redshifts are < 1\n', redsum12);
        end
        if redsum13 > 0
            fprintf(1, '      %4.0f of those redshifts are < 0.3\n', redsum13);
        end
    fprintf(1, '  %4.0f visible GRB(s) within 15 deg zenith\n', j1);
    %fprintf(1, '  %4.0f visible GRB(s) within 15 deg zenith in %s\n', j1, fullFileName);
        if j1 > 0
            fprintf(1, '      %4.0f of these have a known redshift\n', red4);
        end
        if red42 > 0
            fprintf(1, '      %4.0f of those redshifts are < 1\n', red42);
        end
        if red43 > 0
            fprintf(1, '      %4.0f of those redshifts are < 0.3\n', red43);
        end
    %fprintf(1, '  %4.0f almost-visible preceding GRB(s), ~3 hr\n', k3);
    %fprintf(1, '  %4.0f almost-visible preceding GRB(s), ~3 hr, in %s\n', k3, fullFileName);
        %if redsum4 > 0
            %fprintf(1, '      %4.0f of these have a known redshift\n', redsum4);
        %end
        %if redsum42 > 0
            %fprintf(1, '      %4.0f of those redshifts are < 1\n', redsum42);
        %end
        %if redsum43 > 0
            %fprintf(1, '      %4.0f of those redshifts are < 0.3\n', redsum43);
        %end
    %fprintf(1, '  %4.0f almost-visible preceding GRB(s), ~2 hr\n', k4);
    %fprintf(1, '  %4.0f almost-visible preceding GRB(s), ~2 hr, in %s\n', k4, fullFileName);
        %if redsum3 > 0
            %fprintf(1, '      %4.0f of these have a known redshift\n', redsum3);
        %end
        %if redsum32 > 0
            %fprintf(1, '      %4.0f of those redshifts are < 1\n', redsum32);
        %end
        %if redsum33 > 0
            %fprintf(1, '      %4.0f of those redshifts are < 0.3\n', redsum33);
        %end
    fprintf(1, '  %4.0f almost-visible preceding GRB(s), ~1 hr\n', k1);
    %fprintf(1, '  %4.0f almost-visible preceding GRB(s), ~1 hr, in %s\n', k1, fullFileName);
        if k1 > 0
            fprintf(1, '      %4.0f of these have a known redshift\n', red7);
        end
        if red72 > 0
            fprintf(1, '      %4.0f of those redshifts are < 1\n', red72);
        end
        if red73 > 0
            fprintf(1, '      %4.0f of those redshifts are < 0.3\n', red73);
        end
    %fprintf(1, '%4.0f almost-visible trailing GRB(s) in %s\n', n, fullFileName);
    

end

%namesGRB